/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package form.table.model;

import domain.Pisac;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Lazar
 */
public class AutorTableModel extends AbstractTableModel {
    
    private List<Pisac> autori;
    private final Class[] columnTypes=new Class[]{Long.class,String.class,String.class};
    private final String[] columnNames=new String[]{"ID","Ime","Prezime"};
    public AutorTableModel() {
        autori=new ArrayList<>();
        System.out.println("Dodat");
        autori.add(new Pisac(1L, "ime", "Prezime"));
    }

    public AutorTableModel(List<Pisac> autori) {
        this.autori = autori;
    }
    
    @Override
    public int getRowCount() {
        return autori.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

     
    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return columnTypes[columnIndex];
    }

    @Override
    public String getColumnName(int column) {
        return  columnNames[column];//To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object getValueAt(int row, int column) {
        Pisac p=autori.get(row);
        switch(column){
            case 0:return p.getId();
            case 1:return p.getIme();
            case 2:return p.getPrezime();
        }
        throw new ArrayIndexOutOfBoundsException();
    }
    
    

    public List<Pisac> getAutori() {
        return autori;
    }

    public void setAutori(List<Pisac> autori) {
        this.autori = autori;
    }
    
     
    
    
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kontroler;

import form.MainForm;
import form.panel.AutorPanel;
import form.panel.BibliotekaPanel;
import form.panel.KnjigaPanel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JPanel;

/**
 *
 * @author Lazar
 */
public class GUIController {

    private MainForm mainForm;
    private JPanel current;
    private AutorPanel autorPanel;
    private BibliotekaPanel bibliotekaPanel;
    private KnjigaPanel knjigaPanel;

    public GUIController() {

        mainForm = new MainForm();
        mainForm.setVisible(true);
        changePanel(getBibliotekaPanelLazy());
        setActionMainFormActionListeners();

    }

    private AutorPanel getAutorPanelLazy() {
        if (autorPanel == null) {
            autorPanel = new AutorPanel();
        }
        return autorPanel;
    }

    private BibliotekaPanel getBibliotekaPanelLazy() {
        if (bibliotekaPanel == null) {
            bibliotekaPanel = new BibliotekaPanel();
        }
        return bibliotekaPanel;
    }

    private KnjigaPanel getKnjigaPanelLazy() {
        if (knjigaPanel == null) {
            knjigaPanel = new KnjigaPanel();
        }
        return knjigaPanel;
    }

    private void setActionMainFormActionListeners() {

        mainForm.getjMenuAutori().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                changePanel(getAutorPanelLazy());
            }

        });
        mainForm.getjMenuBibiloteke().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                changePanel(getBibliotekaPanelLazy());
            }

        });
        mainForm.getjMenuKnjige().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                changePanel(getKnjigaPanelLazy());
            }

        });
    }

    private void changePanel(JPanel panel) {
        System.out.println("Promena");
        if (current == panel) {
            System.out.println("Isti");
            return;
        }
        mainForm.setContentPane(panel);
        if (current != null) {
            current.setVisible(false);
        }
        panel.setVisible(true);
        current = panel;
        mainForm.pack();
        mainForm.revalidate();
    }
}

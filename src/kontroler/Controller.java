/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kontroler;

import config.Konfiguracija;
import constant.Akcije;
import domain.Pisac;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import transfer.RequestObject;
import transfer.ResponseObject;

/**
 *
 * @author Lazar
 */
public class Controller {
    
    private Socket socket;
    private ObjectOutputStream out;
    private ObjectInputStream in;
    
    private Controller() {
    }
    
    public static Controller getInstance() {
        return ControllerHolder.INSTANCE;
    }

    public List<Pisac> getAutori() {
        try {
            if(socket==null){
                try {
                    createConnection();
                } catch (IOException ex) {
                    socket=null;
                    return null;
                }
            }
            RequestObject req=new RequestObject(Akcije.VRATI_AUTORE);
            out.writeObject(req);
            ResponseObject res=(ResponseObject) in.readObject();
            return (List<Pisac>) res.getData();
        } catch (IOException ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    private static class ControllerHolder {

        private static final Controller INSTANCE = new Controller();
    }
    
    private void createConnection() throws IOException{
        int port=Konfiguracija.getInstance().getPort();
        String host=Konfiguracija.getInstance().getHost();
        socket=new Socket(host, port);
        in=new ObjectInputStream(socket.getInputStream());
        out=new ObjectOutputStream(socket.getOutputStream());
    }
    
}
